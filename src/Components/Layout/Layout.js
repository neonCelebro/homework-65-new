import React, {Fragment} from 'react';
import Navigation from '../Navigation/NavigationItems/NavigationItems';

const Layout = props => (
  <Fragment>
    <nav>
      <Navigation/>
    </nav>
    <main>
      {props.children}
    </main>
  </Fragment>
);
export default Layout;
