import React from 'react';
import { NavLink } from 'react-router-dom';
import './NavigationItem.css';

const NavItem = props => (
  <li className='NavigationItem'>
    <NavLink className='btn' exact={props.exact} to={props.to}>{props.children}</NavLink>
  </li>
)

export default NavItem;
