import React from 'react';
import NavItem from '../NavigationItem/NavigationItem';
import { NavLink } from 'react-router-dom';

const Navigation = (props) => (
  <ul className='NavigationItems'>
    <NavItem to='/AboutUs'>About Us</NavItem>
    <NavItem to='/servises'>servises</NavItem>
    <NavItem to='/faq'>FAQ</NavItem>
    <NavItem to='/blog'>blog</NavItem>
    <NavItem to='/portfolio'>portfolio</NavItem>
    <NavItem to='/admin'>admin</NavItem>
  </ul>
)

export default Navigation;
