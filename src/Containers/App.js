import React, {Component, Fragment} from 'react';
import {Route, NavLink, Switch} from 'react-router-dom';
import Layout from '../Components/Layout/Layout';
import StaticPage from './StaticPage/StaticPage';
import EditStaticPage from './EditStaticPage/EditStaticPage';

export default class App extends Component {
  render() {
    return (<Layout>
      <Switch>
        <Route path='/admin' component={EditStaticPage}/>
        <Route path='/:category' component={StaticPage}/>
      </Switch>
    </Layout>)
  }
};
