import React, {Component, Fragment} from 'react';
import './EditStaticPage.css';
import axios from '../../axios';
import Spinner from '../../UI/Spinner/Spinner';

export default class EditStaticPage extends Component {
  state = {
    category: null,
    title: null,
    content: null,
    loading: false,
    selectCategory: null
  };

  changeSelectCategory = (e) => {
    this.setState({selectCategory: e.target.value})
  }
  changeTitle = (e) => {
    this.setState({title: e.target.value})
  };
  changeContent = (e) => {
    this.setState({content: e.target.value})
  }
  getInfoOnServer = () => {
    const category = {
      ...this.state.category
    };
    category.title = this.state.title;
    category.content = this.state.content;
    this.setState({
      category
    }, () => {
      axios.put(this.state.selectCategory + '.json', this.state.category)
      .then((response) => {
        if (response.status === 200) {
          this.props.history.push(this.state.selectCategory)
        }
      })
    });
  };
  setInfoInState = (e) => {
    e.preventDefault();
    axios.get(this.state.selectCategory + '.json').then((response) => {
      this.setState({category: response.data, title: response.data.title, content: response.data.content});
    });
  };
  render() {
    return (<Fragment>
      <form className='inputs'>
        <select required="required" onBlur={this.changeSelectCategory}>
          <option></option>
          <option value="AboutUs">About Us</option>
          <option value="faq">FAQ</option>
          <option value="servises">Servises</option>
          <option value="blog">Blog</option>
          <option value="portfolio">Portfolio</option>
        </select>
        <button onClick={this.setInfoInState} className='sendNewData'>send</button>
      </form>
      {
        this.state.category
          ? <div className='inputs'>
              <input onChange={this.changeTitle} type='text' className='title' value={this.state.title}/>
              <textarea onChange={this.changeContent} value={this.state.content} className='content'>{this.state.category.content}</textarea>
              <button onClick={this.getInfoOnServer} className='sendNewData sendForm'>Edit</button>
            </div>
          : null
      }
    </Fragment>)
  }
};
