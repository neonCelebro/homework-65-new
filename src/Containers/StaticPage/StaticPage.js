import React, { Component, Fragment } from 'react';
import './StaticPage.css';
import axios from '../../axios';
import Spinner from '../../UI/Spinner/Spinner';
export default class StaticPage extends Component {

  state = {
    category: null,
    loading: false,
};

setInfoInState = () =>{
  this.setState({loading: true}, () => {
    axios.get(this.props.location.pathname + '.json')
    .then( response => this.setState({category: response.data}))
    .finally(this.setState({loading: false}));
});
};
componentDidMount(){
  this.setInfoInState();
};
componentDidUpdate(prevProps){
  if (prevProps.location.key !== this.props.location.key) {
    this.setInfoInState();
  }
};
    render() {
      return (
        this.state.loading ? <Spinner/> :
        this.state.category ? (<div className='wrapper'>
        <h3 className='title'>{this.state.category.title}</h3>
        <p className='content'>{this.state.category.content}</p>
      </div>) : (<div>Not Found!</div>)
    )}
};
