import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://lesson-64-5424e.firebaseio.com/'
});

export default instance;
